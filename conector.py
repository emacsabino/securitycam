import requests
from threading import Thread
import time
tempo_de_envio = True
sair_do_loop=False

def cadastrar_api(nome,email):
    url = 'http://www.softwares.hol.es/api/security_cam/index.php/cadastro'
    dados={'nome':nome,
           'email':email}


    r = requests.post(url,dados)
    return r.text


def retornar_registro(email):
    r = requests.get('http://www.softwares.hol.es/api/security_cam/index.php/registro?email_r='+email)

    return r.text
def contar_registros_iguais(email):
    r = requests.get('http://www.softwares.hol.es/api/security_cam/index.php/qtdregistros?email_r=' + email)


    return r.text


#cadastrar('fernando silva','fernando@hotmail.com')


def enviar_foto(caminho,id):
    global tempo_de_envio

    if tempo_de_envio:
        url = 'http://www.softwares.hol.es/api/security_cam/upload.php/foto'

        files = {'file': ('teste.jpg',open(caminho, 'rb'))}
        data ={'id':id}


        r = requests.post(url, files=files,data=data)



def enviar_foto_completa(caminho,id):
    global tempo_de_envio

    if tempo_de_envio:
        url = 'http://www.softwares.hol.es/api/security_cam/upload.php/foto'

        files = {'file': ('teste.jpg',open(caminho, 'rb'))}
        data ={'id':id}


        r = requests.post(url, files=files,data=data)

        tempo_de_envio=False

def mudanca_tmp_envio():
    global tempo_de_envio
    global sair_do_loop
    while (True):

        if sair_do_loop:

            break
        time.sleep(4)
        if tempo_de_envio==False:
            tempo_de_envio=True


def executar_tempo():
    Thread(target=mudanca_tmp_envio).start()


def apagar_registro(id):
    url = 'http://www.softwares.hol.es/api/security_cam/delete.php/apagar_registro'
    dados = {'id':id}
    r = requests.post(url,dados)



def validar_email(email):
    r = requests.get('http://www.softwares.hol.es/api/security_cam/validacao.php/validar_email?email_r=' + email)
    return r.text


def calcular_tempo(email):
    r = requests.get('http://www.softwares.hol.es/api/security_cam/validade_data.php/validar_data?email_r=' + email)
    return r.text


def get_dt_rg_licenca(email):
    r = requests.get('http://www.softwares.hol.es/api/security_cam/pegar_licenca.php/licenca?email_r=' + email)
    return r.text
#enviar_foto('fotos/teste.jpg')

def mudar_sair_do_loop():
    global sair_do_loop
    sair_do_loop=True

executar_tempo()
