import cv2

from conector import enviar_foto,enviar_foto_completa


def iniciar_vigilancia(camera,id,escal,vizinh,tammin):
    classificador = cv2.CascadeClassifier("reconhecedor/haarcascade-frontalface-default.xml")

    camera = cv2.VideoCapture(camera)


    largura,altura = 220,220
    print("capturando as faces...")
    amostra = 1
    while(True):

        conectado, imagem = camera.read()
        imagemCinza = cv2.cvtColor(imagem,cv2.COLOR_BGR2GRAY)

        facesDetectadas = classificador.detectMultiScale(imagemCinza,scaleFactor=escal,minSize=(tammin,tammin),minNeighbors=vizinh)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        for (x,y,l,a) in facesDetectadas:
            cv2.rectangle(imagem,(x,y),(x+l,y+a),(0,0,255),2)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break



            if x>0:

                imagemFace = cv2.resize(imagem[y:y + a, x:x + l], (largura, altura))
                arquivo = "fotos/pessoa." + str(id) + "." + str(amostra) + ".jpg"
                arquivo2 ="fotos/pessoa." + str(id) + "." + str(amostra) +"corpo"+".jpg"
                cv2.imwrite(arquivo, imagemFace)
                cv2.imwrite(arquivo2,imagem)

                enviar_foto(arquivo,id)
                enviar_foto_completa(arquivo2,id)
                amostra = amostra+1


        cv2.putText(imagem, "para sair aperte a tecla q", (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
                    1.0, (0, 0, 255), 3)
        cv2.imshow("Reconhecedor",imagem)



    camera.release()
    cv2.destroyAllWindows()


def teste_camera(camera,escal,vizinh,tammin):
    classificador = cv2.CascadeClassifier("reconhecedor/haarcascade-frontalface-default.xml")

    camera = cv2.VideoCapture(camera)



    while(True):

        conectado, imagem = camera.read()
        imagemCinza = cv2.cvtColor(imagem,cv2.COLOR_BGR2GRAY)

        facesDetectadas = classificador.detectMultiScale(imagemCinza,scaleFactor=escal,minSize=(tammin,tammin),minNeighbors=vizinh)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        for (x,y,l,a) in facesDetectadas:
            cv2.rectangle(imagem,(x,y),(x+l,y+a),(0,0,255),2)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break







        cv2.putText(imagem, "para sair aperte a tecla q", (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
                    1.0, (0, 0, 255), 3)
        cv2.imshow("Reconhecedor",imagem)



    camera.release()
    cv2.destroyAllWindows()