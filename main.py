from captura import iniciar_vigilancia,teste_camera

from conector import *
from banco.db import *
from kivy.lang.builder import Builder
from kivy.uix.screenmanager import ScreenManager,Screen
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.app import App
from kivy.clock import Clock
import cv2
import socket
import sys


from kivy.core.window import Window
Window.clearcolor = (0.6, 0.7, 0.6, 1)





class Gerenciador(ScreenManager):
    criandoTabelaRegistros()
    criandoTabelaFotos()
    criandoTabelaConfiguracoes()
    criandoTabelaCameras()
    criandoTabelaDataIncialLicenca()












class Menu(Screen):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        Clock.schedule_once(self.on_start)


    def sair(self):

        mudar_sair_do_loop()

        App.get_running_app().stop()








    def on_start(self,*args):
        self.conectou()

    def pegar_dados_smart(self):

        verifica = verificar_registros()
        if verifica==0:
            label = Label(text='Você ainda não criou um usuário', font_size=20)

            box = BoxLayout(orientation='vertical')
            box.add_widget(label)


            alerta = Popup(title='ALERTA!', content=box, size_hint=(None, None),
                           size=(400, 300))
            alerta.auto_dismiss = False
            btn = Button(text='OK', on_release=alerta.dismiss)
            box.add_widget(btn)
            alerta.open()
        else:
            registro = get_email_registro()
            email = registro[0][1]
            reg = registro[0][2]

            label = Label(text='Os dados para usar no smartphone, são:\nEMAIL: '+str(email)+'\nCHAVE: '+str(reg))

            box = BoxLayout(orientation='vertical')
            box.add_widget(label)

            alerta = Popup(title='Dados para o smartphone!', content=box, size_hint=(None, None),
                           size=(400, 300))
            alerta.auto_dismiss = False
            btn = Button(text='OK', on_release=alerta.dismiss)
            box.add_widget(btn)
            alerta.open()

    def conectou(self):
        if self.verifica_conexao():
            pass

        else:
            bx =self.ids.wg_pai
            bx.clear_widgets()
            bx.add_widget(Image(source='logos/sem_internet.png'))
            bx.add_widget(Label(text='Você não está conectado a internet, verifique sua conexão\n'
                                     'e execute novamente o programa'))


    def verifica_conexao(self):
        confiaveis = ['www.google.com', 'www.yahoo.com', 'www.bb.com.br']
        for host in confiaveis:
            a = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            a.settimeout(.5)
        try:
            b = a.connect_ex((host, 80))
            if b == 0:  # ok, conectado
                return True
        except:
            pass
            a.close()
            return False




class Vigilancia(Screen):

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def camerasAtivas(self):

        numero = 10
        lista=[]
        for i in range(0,numero):
            camera = cv2.VideoCapture(i)
            retorno = camera.open(i)


            if retorno:
                lista.append(str(i))
                inserirCamera(int(i))
            camera.release()

        cv2.destroyAllWindows()
        return lista
    def lista(self):
        lis = self.camerasAtivas()
        self.ids.quantidade_ativas.text="Seu computador possui "+str(len(lis))+" webcam(s) ativa(s) no momento"
        self.ids.quantidade_ativas.color=(0.4,0.2,0.8,1)

        return lis


    def spinnerValores(self):


        self.ids.spinner_camera.values=self.lista()
    def textoPadrao(self):

        self.ids.quantidade_ativas.text = "aguarde...sistema está detectando quantas webcams estão ativas!"
        self.ids.quantidade_ativas.color = (1, 0.7, 0, 1)

    def iniciar_vigilancia(self):

        id_r = retornar_id_registro()

        if len(id_r)==0:
            label = Label(text='Você ainda não criou um usuário', font_size=20)

            box = BoxLayout(orientation='vertical')
            box.add_widget(label)

            alerta = Popup(title='ALERTA!', content=box, size_hint=(None, None),
                           size=(400, 300))
            alerta.auto_dismiss = False
            btn = Button(text='OK', on_release=alerta.dismiss)
            box.add_widget(btn)
            alerta.open()
        else:

            dado_email = get_email_registro()
            email_reg = dado_email[0][1]
            dias = int(calcular_tempo(email_reg))

            if dias>365:

                self.alerta_msg("Sua licença de uso pelo período de 1 ano, chegou ao fim\n"
                                "renove sua licença ou contrate uma nova conta, entre em contato\n"
                                "pgmacnet@hotmail.com ou visite nosso site\nhttp://maclaine.net/securitycam",
                                "SUA LICENÇA ANUAL EXPIROU!!")
            else:

                minsize, vizinhan, escala = pegarConfiguracao()



                camera_escolhida = int(self.ids.spinner_camera.text)
                iniciar_vigilancia(camera_escolhida,id_r[0][0],float(escala),int(vizinhan),int(minsize))

    def alerta_msg(self,texto, titulo):
        label = Label(
            text=texto,font_size=14)

        box = BoxLayout(orientation='vertical')
        box.add_widget(label)

        alerta = Popup(title=titulo, content=box, size_hint=(None, None),
                       size=(500, 300))
        alerta.auto_dismiss = False
        btn = Button(text='OK', on_release=alerta.dismiss)
        box.add_widget(btn)
        alerta.open()

class ConfigurarCam(Screen):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)
    def get_valores_config(self):
        minsize, vizinhan, escala = pegarConfiguracao()
        self.ids.input_tam_min.text=str(minsize)
        self.ids.input_vizinhanca.text=str(vizinhan)
        self.ids.input_escala.text=str(escala)
        self.ids.spinner_camera.values = pegarCameras()
    def salvar_configuracao(self):
        tam = int(self.ids.input_tam_min.text)
        viz = int(self.ids.input_vizinhanca.text)
        scal = float(self.ids.input_escala.text)

        if scal<1.1:
            self.alerta_msg('O valor do campo escala tem que ser maior ou \nigual a 1.1','Atenção')
            return None
        if tam<30:
            self.alerta_msg('O valor para tamanho mínimo não pode ser \nmenor que 30','Atenção')
            return None




        salvarConfiguracao(tam,viz,scal)

        label = Label(text='Os dados foram salvos com sucesso,:\ntamanho_minimo: ' + str(tam) + '\nvizinhança: ' + str(viz)
                           +'\nescala: '+str(scal))

        box = BoxLayout(orientation='vertical')
        box.add_widget(label)

        alerta = Popup(title='SUCESSO!', content=box, size_hint=(None, None),
                       size=(400, 300))
        alerta.auto_dismiss = False
        btn = Button(text='OK', on_release=alerta.dismiss)
        box.add_widget(btn)
        alerta.open()

    def restaurar_padrao(self):
        salvarConfiguracao(30,1,1.5)
        self.ids.input_tam_min.text = str(30)
        self.ids.input_vizinhanca.text = str(1)
        self.ids.input_escala.text = str(1.5)
        label = Label(
            text='Os valores padrões foram restaurados')


        box = BoxLayout(orientation='vertical')
        box.add_widget(label)

        alerta = Popup(title='SUCESSO!', content=box, size_hint=(None, None),
                       size=(400, 300))
        alerta.auto_dismiss = False
        btn = Button(text='OK', on_release=alerta.dismiss)
        box.add_widget(btn)
        alerta.open()
    def testar_camera(self):
        tam = int(self.ids.input_tam_min.text)
        viz = int(self.ids.input_vizinhanca.text)
        scal = float(self.ids.input_escala.text)
        camera = int(self.ids.spinner_camera.text)
        if scal<1.1:
            self.alerta_msg('O valor do campo escala tem que ser maior ou \nigual a 1.1','Atenção')
            return None
        if tam<30:
            self.alerta_msg('O valor para tamanho mínimo não pode ser \nmenor que 30','Atenção')
            return None



        teste_camera(camera,scal,viz,tam)

    def alerta_msg(self,texto, titulo):
        label = Label(
            text=texto)

        box = BoxLayout(orientation='vertical')
        box.add_widget(label)

        alerta = Popup(title=titulo, content=box, size_hint=(None, None),
                       size=(400, 300))
        alerta.auto_dismiss = False
        btn = Button(text='OK', on_release=alerta.dismiss)
        box.add_widget(btn)
        alerta.open()

    def apagar_cameras(self):
        apagarCamera()


class Newuser(Screen):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def valid_cmp_branco(self,valor):
        if valor.strip() == '':
            return  False


        return True

    def valid_email(self,string):

        pos = string.find("@")
        dot = string.rfind(".")
        if pos < 1:
            return False
        if dot < pos + 2:
            return False
        if dot + 2 >= len(string):
            return False
        return True

    def inserir_registro(self):
        nome = self.ids.nome.text
        email = self.ids.email.text
        email2 = email.lstrip()
        if not(self.valid_cmp_branco(nome)):
            label2 = Label(
                text='O campo nome está em branco, preencha-o')
            box = BoxLayout(orientation='vertical')

            box.add_widget(label2)

            alerta = Popup(title='ALERTA!', content=box, size_hint=(None, None),
                           size=(400, 300))
            alerta.auto_dismiss = False
            btn = Button(text='OK', on_release=alerta.dismiss)
            box.add_widget(btn)

            alerta.open()
            return None

        if not(self.valid_email(email2)):
            label2 = Label(
                text='Coloque um email válido')
            box = BoxLayout(orientation='vertical')

            box.add_widget(label2)

            alerta = Popup(title='ALERTA!', content=box, size_hint=(None, None),
                           size=(400, 300))
            alerta.auto_dismiss = False
            btn = Button(text='OK', on_release=alerta.dismiss)
            box.add_widget(btn)

            alerta.open()
            return None


        if self.validar_usuario(email2):
            if (self.controlar_registros_iguais(email2)):
                id = cadastrar_api(nome,email2)
                print("Cadastro_id_cliente "+id)
                registro =retornar_registro(email2)
                inserirCadastroLocal(id,registro,email2)

                label = Label(text='Usuário criado com sucesso!',font_size=20)
                label2 = Label(text='Os dados para serem usados no seu smartphone são:\nEMAIL: '+str(email2)+'\nCHAVE: '+str(registro))
                box = BoxLayout(orientation='vertical')
                box.add_widget(label)
                box.add_widget(label2)

                alerta = Popup(title='SUCESSO!', content=box, size_hint=(None, None),
                               size=(400, 300))
                alerta.auto_dismiss=False
                btn = Button(text='OK', on_release=alerta.dismiss)
                box.add_widget(btn)
                nome = self.ids.nome.text = ''
                email = self.ids.email.text = ''
                alerta.open()
                self.manager.current = 'menu'

    def controlar_new_user(self):
        qtd = verificar_registros()
        if qtd > 0:
            id_r = retornar_id_registro()


            label = Label(text='Você já tem um registro criado.Para criar um novo\nregistro, o anterior será excluído')
            label2 = Label(text='Gostaria de excluir o registro antigo e criar um novo?')
            box = BoxLayout(orientation='vertical')
            box.add_widget(label)
            box.add_widget(label2)

            alerta = Popup(title='ALERTA!', content=box, size_hint=(None, None),
                           size=(400, 300))
            alerta.auto_dismiss=False
            btn = Button(text='SIM')
            btn.fbind('on_release', lambda e: self.zerar_tabela(alerta,id_r))
            btn2 = Button(text='CANCELAR')
            btn2.fbind('on_release', lambda e: self.voltar(alerta))
            box.add_widget(btn)
            box.add_widget(btn2)
            alerta.open()

    def zerar_tabela(self,tela,id):
        apagar_registro(id)
        tela.dismiss()
        limpar_tabela_registros()
        apagarLicenca()

    def voltar(self,tela):
        self.manager.current = 'menu'
        tela.dismiss()
    def controlar_registros_iguais(self,email):
        retorno = True
        valor = int(contar_registros_iguais(email))
        if valor>=3:
            label = Label(text='Não foi possível criar um novo registro, pois você já possui \ntrês clientes ativos, para solucionar isso, você pode:\n\n\n'
                          +'1 - Adquira outro produto, assim você poderá criar mais três registros\n'
                          +'2 - Caso não queira comprar um novo produto, você pode excluir os clientes \ne criar outros para substituir\n'
                          +'3 - Qualquer dúvida ou algum problema, entre em \ncontato pelo email: emacsabino@hotmail.com')

            box = BoxLayout(orientation='vertical')
            box.add_widget(label)

            alerta = Popup(title='Atenção!', content=box, size_hint=(None, None),
                           size=(600, 450))
            alerta.auto_dismiss = False
            btn = Button(text='OK', on_release=alerta.dismiss)
            box.add_widget(btn)
            alerta.open()



            retorno=False


        return retorno
    def validar_usuario(self,email):
        retornar = True
        qtd = int(validar_email(email))

        if qtd==0:
            label = Label(
                text='Seu registro não foi validado, você precisa adquirir licensa \npara uso do software, veja suas opções:\n\n'
                     + '1 - Você pode adquirir o produto entrando em contato \natravés do email: emacsabino@hotmail.com\n'
                     + '2 - Visite o site oficial do produto: www.maclaine.net\securitycam\n'
                     + '3 - Qualquer dúvida ou algum problema, entre em \ncontato pelo email: emacsabino@hotmail.com')

            box = BoxLayout(orientation='vertical')
            box.add_widget(label)

            alerta = Popup(title='Atenção!', content=box, size_hint=(None, None),
                           size=(600, 450))
            alerta.auto_dismiss = False
            btn = Button(text='OK', on_release=alerta.dismiss)
            box.add_widget(btn)
            alerta.open()
            retornar = False

        else:
            dat = get_dt_rg_licenca(email)
            inserirLicenca(email,dat)
            #aqui eu implemento a data inicial da licença, para expiração

        return retornar






class SecurityCam(App):
    def build(self):
        Builder.load_string(open("interface.kv",encoding='utf8').read(),rulesonly=True)
        self.icon='logos/icone_security.png'
        email, data = pegarLicenca()
        if email!='':
            App.title='SecurityCam - Licenca: '+email+' iniciada em '+data
        return Gerenciador()
    def on_stop(self):
        mudar_sair_do_loop()






SecurityCam().run()













