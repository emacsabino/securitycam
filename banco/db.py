import sqlite3

import os


def criandoTabelaRegistros():
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS registros (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            email VARCHAR(120),
            id_registro BIGINT,
            codigo_verificador VARCHAR(500)

    );
    """)
    conex.close()

def criandoTabelaCameras():
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS camera (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            n_cam INTEGER UNIQUE
            

    );
    """)
    conex.close()

def criandoTabelaDataIncialLicenca():
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS data_licenca (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                email TEXT,
                data VARCHAR(20)


        );
        """)
    conex.close()

def inserirLicenca(email,data):
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()

    cursor.execute("""
            INSERT INTO data_licenca (email,data)
            VALUES (?,?)
            """, (email,data))

    conex.commit()
    conex.close()
def apagarLicenca():
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()
    cursor.execute("""
        DELETE FROM data_licenca
        """)
    conex.commit()
    conex.close()

def pegarLicenca():
    conn = sqlite3.connect("Banco/registros.db")
    cursor = conn.cursor()

    # lendo os dados
    cursor.execute("""
                SELECT email,data FROM data_licenca;
                """)

    lic = cursor.fetchall()
    if len(lic)>0:
        em = lic[0][0]
        dta = lic[0][1]
    else:
        em=''
        dta=''


    conn.close()
    return em,dta


def inserirCamera(numero):

    conex = sqlite3.connect("Banco/registros.db")
    cursor =conex.cursor()

    cursor.execute("""
        INSERT OR IGNORE INTO camera (n_cam)
        VALUES (?)
        """,(numero,))


    conex.commit()
    conex.close()

def apagarCamera():
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()
    cursor.execute("""
    DELETE FROM camera
    """)
    conex.commit()
    conex.close()



def pegarCameras():
    lis_cam = []
    conn = sqlite3.connect("banco/registros.db")
    cursor = conn.cursor()

    # lendo os dados
    cursor.execute("""
            SELECT n_cam FROM camera;
            """)

    lista = cursor.fetchall()
    for val in range(0,len(lista)) :

        lis_cam.append(str(val))


    conn.close()
    return lis_cam


def criandoTabelaFotos():
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS fotos (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            caminho TEXT NOT NULL,
            timestamp DATETIME DEFAULT CURRENT_TIMESTAMP

    );
    """)
    conex.close()


def criandoTabelaConfiguracoes():
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS configuracao(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    min_size INTEGER,
    vizinhanca INTEGER,
    scale_factor REAL   
    );
    """)
    cursor.execute("""
    SELECT COUNT(*) FROM configuracao""")
    valor = cursor.fetchall()

    if int(valor[0][0])==0:


        padrao_min_size = 30
        padrao_vizinhanca=1
        padrao_scale_factor = 1.5
        cursor.execute("""
        INSERT INTO configuracao (min_size,vizinhanca,scale_factor) 
        VALUES(?,?,?)
        """,(padrao_min_size,padrao_vizinhanca,padrao_scale_factor))
        conex.commit()

    conex.close()
def pegarConfiguracao():
    conn = sqlite3.connect("Banco/registros.db")
    cursor = conn.cursor()

    # lendo os dados
    cursor.execute("""
            SELECT min_size,vizinhanca,scale_factor FROM configuracao;
            """)

    configuracao = cursor.fetchall()
    minsize = configuracao[0][0]
    vizinhan = configuracao[0][1]
    escala = configuracao[0][2]

    conn.close()
    return minsize,vizinhan,escala
def salvarConfiguracao(minimo,vizinh,scale):
    conn = sqlite3.connect("Banco/registros.db")
    cursor = conn.cursor()

    # lendo os dados
    cursor.execute("""
            UPDATE configuracao SET min_size=?,vizinhanca=?,scale_factor=? WHERE id=?;
            """,(minimo,vizinh,scale,1))

    conn.commit()

    conn.close()

def inserirCadastroLocal(p_id,p_registro,p_email):
    verificador = verificar_registros()
    conex = sqlite3.connect("Banco/registros.db")
    cursor =conex.cursor()
    if verificador==0:
        cursor.execute("""
        INSERT INTO registros (id_registro,email, codigo_verificador)
        VALUES (?,?,?)
        """, (p_id,p_email,p_registro))
    else:
        cursor.execute("""
            UPDATE registros
            SET id_registro=?, email=?, codigo_verificador=?
            """, (p_id,p_email,p_registro))


    conex.commit()
    conex.close()


def verificar_registros():
    conex = sqlite3.connect("Banco/registros.db")
    cursor = conex.cursor()
    cursor.execute("""
        SELECT COUNT(*) FROM registros""")
    valor = cursor.fetchall()

    conex.close()
    return int(valor[0][0])

def get_email_registro():


    conn = sqlite3.connect("Banco/registros.db")
    cursor = conn.cursor()

    # lendo os dados
    cursor.execute("""
        SELECT id_registro,email,codigo_verificador FROM registros;
        """)

    lista = cursor.fetchall()

    conn.close()

    return lista

def limpar_tabela_registros():
    conn = sqlite3.connect("Banco/registros.db")
    cursor = conn.cursor()

    # lendo os dados
    cursor.execute("""
            DELETE FROM registros
            """)

    conn.commit()
    conn.close()

def retornar_id_registro():
    conn = sqlite3.connect("Banco/registros.db")
    cursor = conn.cursor()

    # lendo os dados
    cursor.execute("""
            SELECT id_registro FROM registros;
            """)

    id_r = cursor.fetchall()


    conn.close()
    return id_r

